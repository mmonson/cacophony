# Cacophony
This repository is one half of a capstone project at the end of my 2-year app developer cert program at Renton Technical College. Specifically, it is the server part of the client-server application.
You can find the client-facing code here: 
[mmonson/cacophony-vue](https://gitlab.com/mmonson/cacophony-vue)

![Python Logo](/docs/python-logo.png "Python Logo") 
![Vue Logo](/docs/vue-logo.png "Vue Logo") 
![Flask Logo](/docs/flask-logo.png "Flask Logo")

# Project Goals

The goal of this project was to mimic the core messaging functionality common to applications such as Matrix, Discord, Skype and Slack. It features "servers" as groups of users, with the creator of the server as the administrator for that group. Users can send messages to any number of channels on that server. They can also delete and edit their own messages. If they are the server's creator, they can also delete other user's messages. The full proposal is located in [docs/PROPOSAL.md](/docs/PROPOSAL.md).

# Post-mortem
## What went well:
Vue itself, once I got my dev environment up and running reliably, was (mostly) a joy to learn and work with. If I had more opportunities to write web applications, I would probably choose it as my framework again.

Nearly all of the proposed interface, messaging and server functionality is implemented and functional. Given a bit more TLC, it could function acceptably for small-scale organizations. It would not scale very well, however, as that was not within the scope of this project. 

## What didn't go well:
The project was not completed fully. I encountered more technical problems and spent much more time troubleshooting than I anticipated. As such, while the core functionality is all there, but some features like user mentions and notifications did not happen.

I had many, many technical problems with this project. Aside from problems with not understanding the dev environment and tools (yarn, babel, **webpack** in particular), the most frustrating and time consuming problem was a bug I introduced related to CORS, where two libraries were setting up CORS headers differently and one was overwriting the other. It took me far longer to find the true cause of the problem than I would like to admit.

Since I never set up a web servlet (gunicorn or gevent) to host the Flask web server, and the development server that I did use has a tendency to freeze during runtime, it sometimes just locks up and there is not very good UI feedback for the client when this happens.