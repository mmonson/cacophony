#!/bin/bash
echo Running release script...
FLASK_APP='run.py'
pip install -r requirements.txt
flask db upgrade
# cd app/client/vue_app
# npm install
# npm install -g @vue/cli @vue/cli-service-global
# vue build src/main.js
echo Release script complete.
