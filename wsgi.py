from app import create_app
from app.config import Config
import subprocess, os

subprocess.call("npm run build", cwd=Config.CLIENT_DIR, shell=True)

app = create_app()
