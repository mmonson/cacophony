import os
import urllib.request as urllib
from libgravatar import Gravatar


def get_gravatar_url(email: str):
    print('Getting gravatar for user')
    g = Gravatar(email)
    return g.get_image(size=40, use_ssl=True, rating='pg', default='identicon')


def store_gravatar(user: 'User') -> str:
    g = get_gravatar_url(user.email)
    avatar_store = os.path.join('app', 'data', 'avatars')
    if not os.path.isdir(avatar_store):
        os.makedirs(avatar_store)
    path = os.path.join('data', 'avatars', str(user.id) + '.jpeg')
    return path
