from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, current_user
from flask_socketio import SocketIO, join_room, leave_room, rooms, emit
# from .socket import socketio

db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
socketio = SocketIO()

# TODO: find a way to move this socketio stuff out


@socketio.on('connect')
def login_handler():
    user = current_user
    if current_user.is_authenticated:
        for server in user.servers:
            join_room(server.id)
        print('user ' + str(user.id) + ' subscribed to their servers')
        emit('test')


@socketio.on('disconnected')
def logout_handler():
    user = current_user
    if current_user.is_authenticated:
        for room in rooms():
            leave_room(room)
        print('user ' + str(user.id) + ' unsubscribed to their servers')
