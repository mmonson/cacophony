from flask_restplus import Resource, abort
from flask_login import current_user

from app.api import api_rest as rest
from app.api.security import require_auth, SecureResource
from app.models import Server
from flask import request

id_parser = rest.parser()
id_parser.add_argument('server_id', required=True, location='args')

id_json_parser = rest.parser()
id_json_parser.add_argument('server_id', required=True, location='json')

create_parser = rest.parser()
create_parser.add_argument('server_name', required=True, location='json',
                           trim=True)

del_parser = rest.parser()
del_parser.add_argument('server_id', type=int, required=True,
                        location='args')


@rest.route('/manage_server')
class ServerManage(Resource):
    @require_auth
    @rest.expect(del_parser)
    def delete(self):
        '''Delete existing server'''
        server_id = del_parser.parse_args()['server_id']
        s = Server.get(server_id)
        if s:
            s.destroy()
            return {'success': True}, 200
        return None, 404

    @require_auth
    @rest.expect(create_parser)
    def post(self):
        if current_user.is_authenticated:
            user = current_user
            if user is not None:
                # make the server, with the user as its owner
                parsed = create_parser.parse_args(strict=True)
                name = parsed['server_name']
                if not (1 <= len(name) <= 100):
                    return abort(400)
                s = Server.create(user, name)
                if s is not None:
                    user.join_server_obj(s)
                    return {'server_id': s.id, 'server_name': s.name}, 201
                else:
                    return abort(500)



@rest.route('/server/channel')
class GetChannels(SecureResource):
    @rest.expect(id_parser)
    def get(self):
        if not current_user.is_authenticated:
            return abort(401)
        server_id = id_parser.parse_args(strict=True)['server_id']
        s = Server.query.get(server_id)

        if s is None:
            return '', 404

        if s.is_authorized(current_user, 'see-channels'):
            channels = s.channels.all()
            if len(channels) == 0:
                return {'error': 'No channels for server. Please report this.'}, 204
            ret = [{'name': c.name} for c in channels]
            return {'channels': ret}
        return abort(401)


@rest.route('/invite')
class ServerInvite(Resource):
    @rest.expect(id_parser)
    def get(self, server_id):
        server_id = id_parser.parse_args(strict=True)['server_id']
        u = current_user
        s = Server.query.get(server_id)
        if s is not None and u is not None:
            if u.join_server_obj(server_id):
                return {'success': True}
            else:
                return {'success': False}

    def post(self, server_id):
        print(request.get_json())
        server_id = id_json_parser.parse_args(strict=True)['server_id']
        u = current_user
        s = Server.query.get(server_id)
        if s is not None and u.is_authenticated:
            if u.join_server_obj(server_id):
                return {'success': True}
            else:
                return {'success': False}
