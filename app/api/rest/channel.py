from flask_login import current_user
from flask_restplus import Resource, abort
from flask import request
from app.api import api_rest as rest
from app.api.security import SecureResource, require_auth
from app.models import Channel, Server, User, Message
from app import socketio

channel_id_parser = rest.parser()
channel_id_parser.add_argument('server_id', type=int, required=True,
                               location='json')
channel_id_parser.add_argument('channel_name', required=True,
                               location='json', trim=True)

get_parser = rest.parser()
get_parser.add_argument('server', type=int, required=True,
                        location='args')
get_parser.add_argument('channel', required=True,
                        location='args', trim=True)

message_parser = rest.parser()
message_parser.add_argument('server', type=int, required=True,
                            location='json')
message_parser.add_argument('channel', required=True,
                            location='json', trim=True)
message_parser.add_argument('content', required=True,
                            location='json', trim=True)

id_parser = rest.parser()
id_parser.add_argument('id', type=int, required=True, location='args')


@rest.route('/channel')
class ChannelRoute(SecureResource):
    @rest.expect(channel_id_parser)
    def post(self):
        '''Create new channel.'''
        parsed = channel_id_parser.parse_args(strict=True)
        channel_name = parsed.channel_name
        server = Server.query.get(parsed.server_id)
        creator = current_user

        valid_ids = server is not None and creator is not None

        if not valid_ids and not (Channel.MIN_NAME_LEN <= channel_name <=
           Channel.MAX_NAME_LEN):
            return {'success': False}

        if not server.is_authorized(creator, 'manage-channel'):
            return abort(401)

        existing_channel = Channel.query.get((server.id, channel_name))
        if existing_channel is not None:
            return abort(409)

        c = Channel.create(server, channel_name)
        if c is not False:
            return {'success': True}, 201


@rest.route('/channel/initmessages')
class InitMessages(SecureResource):
    @rest.expect(get_parser)
    def get(self):
        '''Get messages for channel.'''
        parsed = get_parser.parse_args(strict=True)
        channel_name = parsed.channel
        server = Server.query.get(parsed.server)
        user = current_user
        channel = Channel.query.get((server.id, channel_name))

        if not server.is_authorized(user, 'read-channel', channel):
            return abort(401)

        if channel is None:
            return abort(404)

        msgs = channel.messages.order_by(Message.created)\
                               .limit(100).all()
        if len(msgs) == 0:
            return {'success': True, 'messages': []}

        msgs = [m.serialize() for m in msgs]

        return {'success': True, 'messages': msgs}


@rest.route('/channel/message')
class MessageRoute(SecureResource):
    @rest.expect(message_parser)
    def post(self):
        '''Handle message sent from user.'''
        parsed = message_parser.parse_args(strict=True)
        content = parsed.content
        server = parsed.server
        channel = parsed.channel

        server = Server.query.get(server)
        channel = Channel.query.get((server.id, channel))

        if channel is None:
            return abort(404)

        m = Message()
        m.compose(channel, current_user, content)

        socketio.emit('message', {
                     'server': server.id,
                     'channel': channel.name,
                     'message': m.serialize()
                     },
                     room=server.id)
        if m is not None:
            return {'success': True, 'message': m.serialize()}

    def delete(self):
        parsed = id_parser.parse_args(strict=True)
        id = parsed.id
        m = Message.query.get(id)

        if m is not None:
            if (m.author_id == current_user.id) or \
               (m.server.owner == current_user.id):
                if m.destroy():
                    socketio.emit('message-delete', {
                        'server': m.server,
                        'channel': m.channel,
                        'message_id': id
                    })


