from flask import escape, send_file
from flask_login import current_user, login_user, logout_user
from flask_restplus import Resource, abort
from werkzeug.security import generate_password_hash
from libgravatar import Gravatar

from app.api import api_rest as rest
from app.api.security import require_auth
from app.models import Server, User
import os, urllib.request

auth_parser = rest.parser()
auth_parser.add_argument('email', required=True, location='json', trim=True)
auth_parser.add_argument('password', required=True, location='json')

register_parser = rest.parser()
register_parser.add_argument('username', required=True, location='json',
                             trim=True)
register_parser.add_argument('email', required=True, location='json',
                             trim=True)
register_parser.add_argument('password', required=True, location='json')


@rest.route('/authenticate')
class Authenticate(Resource):
    def get(self):
        """Get the authentication state for this session."""
        if current_user.is_authenticated:
            return {'success': True, 'username': escape(current_user.username),
                    'user_id': current_user.id}
        else:
            return {'success': False}, 401

    @rest.expect(auth_parser)
    def post(self):
        """Authenticate as a user."""
        parsed = auth_parser.parse_args(strict=True)
        email = parsed['email']
        password = parsed['password']
        # return value of a with_entities query is a tuple
        user = \
            User.query.filter(User.email == email).first()
        if user is not None:
            if user.check_password(password):
                login_user(user)
                return {'success': True}
        return abort(401)

    def delete(self):
        """Log out."""
        logout_user()
        assert(not current_user.is_authenticated)
        return {'success': True}


@rest.route('/register', methods=['POST'])
class Register(Resource):
    @rest.expect(register_parser)
    def post(self):
        # If parse_args finds a missing required param, method ends
        valid = True
        response = {'errors': {'username': '', 'password': '', 'email': ''}}
        form = register_parser.parse_args()

        username, email, password = \
            form['username'], form['email'], form['password']

        unamelen = len(username)
        if unamelen < 3 or unamelen > 30:
            response['errors']['username'] = \
                'Invalid username length. Must be between 3 and 30 characters.'
            valid = False

        elif (User.query.filter(User.username == username)
              .with_entities(User.username).first() is not None):
            response['errors']['username'] = 'Username already taken.'
            valid = False

        if (User.query.filter(User.email == email)
                .with_entities(User.email).first() is not None):
            response['errors']['email'] = 'Email already in use.'
            valid = False

        response['success'] = valid

        if not valid:

            return response, 400
        else:
            # try to make registration happen
            hashed = generate_password_hash(password)
            success = User.register(username, email, hashed)
            response['success'] = success
            return response


@rest.route('/avatar/<int:user_id>')
class Avatar(Resource):
    # def post(self):
    #     # todo
    def get(self, user_id):
        '''Retrieve avatar for display.'''
        u = User.query.get(user_id)
        if u is not None:
            if u.avatar is None:
                u.retrieve_gravatar_and_store()

            g = Gravatar(u.email)
            a = urllib.request.urlretrieve(g.get_image(size=40, use_ssl=True, rating='pg', default='identicon'), 'app/data/avatars/' + str(u.id) + '.jpeg')
            return send_file('data/avatars/' + str(u.id) + '.jpeg')


@rest.route('/user/server_list')
class GetServerList(Resource):

    @require_auth
    def get(self):
        '''Get a list of servers and their names that you,
        the authenticated user, belong to.'''
        s = current_user.servers.with_entities(Server.id, Server.name, Server.owner_id).all()
        s = [{'id': i[0], 'name': i[1], 'owner': i[2]} for i in s]
        return {'success': True, 'servers': s}
