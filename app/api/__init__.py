""" API Blueprint Application """
import datetime
from flask import Blueprint, session
from flask_restplus import Api

api_bp = Blueprint('api_bp', __name__,
                   template_folder='templates',
                   url_prefix='/api')

api_rest = Api(api_bp)


@api_bp.before_request
def keep_session():
    session.permanent = True
    api_bp.permanent_session_lifetime = datetime.timedelta(minutes=20)
    session.modified = True  # keeps session alive


@api_bp.after_request
def add_header(response):
    # response.headers['Access-Control-Allow-Headers'] = 'Content-Type,Authorization'
    # # response.headers['Access-Control-Allow-Origin'] = 'http://localhost:8080'
    # response.headers['Access-Control-Allow-Credentials'] = 'true'
    # response.headers['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS, PUT, DELETE'

    # Required for Webpack dev application to make  requests to flask api
    # from another host (localhost:8080)
    # if not current_app.config['PRODUCTION']:
    #     response.headers['Access-Control-Allow-Origin'] = '*'
    return response

from app.api.rest import user, server, channel
