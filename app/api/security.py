""" Security Related things """
from functools import wraps

from flask_login import current_user
from flask_restplus import Resource, abort


def require_auth(func):
    """ Secure method decorator """
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Verify if User is Authenticated
        # Authentication logic goes here
        if current_user.is_authenticated:
            return func(*args, **kwargs)
        else:
            return abort(401)
    return wrapper


class SecureResource(Resource):
    method_decorators = [require_auth]
