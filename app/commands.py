import os
import subprocess

import click

from .config import Config

CLIENT_DIR = Config.CLIENT_DIR


def _bash(cmd, **kwargs):
    """ Helper Bash Call"""
    click.echo('>>> {}'.format(cmd))
    return subprocess.call(cmd, env=os.environ, shell=True, **kwargs)


@click.group()
def client():
    """Vue client commands."""
    pass


@client.command(help='Run the Vue client server.')
def serve():
    click.echo('Serving client...')
    _bash('npm run serve', cwd=CLIENT_DIR)


@client.command(help='Build the Vue client for production usage.')
def build():
    _bash('npm run build', cwd=CLIENT_DIR)
    click.echo('Build complete')
