# from flask_socketio import SocketIO as socketio
from flask_socketio import join_room, leave_room, emit, rooms
from flask_login import current_user
from flask_socketio import SocketIO

socketio = SocketIO()


@socketio.on('connected')
def login_handler(data):
    user = current_user
    for server in user.servers:
        join_room(server.id)
    print('user ' + user.id + ' subscribed to their servers')
    emit('test')


@socketio.on('disconnected')
def logout_handler(data):
    user = current_user
    for room in rooms():
        leave_room(room)
    print('user ' + user.id + ' unsubscribed to their servers')

