from flask import Flask
from . import config
from app.extensions import db, migrate, login_manager, socketio
from app import commands
from flask_cors import CORS
from app.api import api_bp
from app.client import client_bp


def create_app():
    app = Flask(__name__)
    flask_config = config.configure(app)
    register_blueprints(app)
    register_extensions(app)
    register_commands(app)

    app.logger.info('>>> {}'.format(flask_config))
    return app


def register_blueprints(app):
    app.register_blueprint(api_bp)
    app.register_blueprint(client_bp)
    app.logger.debug('Registered routing blueprints')


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    app.db = db
    socketio.init_app(app, cors_credentials=True, ping_interval=10)
    app.socketio = socketio

    login_manager.init_app(app)
    CORS(app, supports_credentials=True)
    app.logger.debug('Registered app extensions')


def register_commands(app):
    app.cli.add_command(commands.client)
    app.logger.debug('Registered CLI commands')
