from datetime import datetime
from typing import Union

from flask_login import UserMixin
from sqlalchemy.dialects.mysql import BIGINT
from sqlalchemy.exc import IntegrityError
from werkzeug.security import check_password_hash

from app import login_manager

from . import gravatar
from .extensions import db

tbl_user_server = \
    db.Table('UserServer',
             db.Column('user_id', db.Integer,
                       db.ForeignKey('user.id'),
                       primary_key=True),
             db.Column('server_id', db.Integer,
                       db.ForeignKey('server.id'),
                       primary_key=True),
             db.UniqueConstraint('user_id', 'server_id',
                                 name="unique_userserver"),
             extend_existing=True,
             )


class DestroyableModel(object):
    def destroy(self):
        try:
            self.delete()
            db.session.flush()
            db.session.commit()
            return True
        except Exception as ex:
            raise ex


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(30), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(93))
    avatar = db.Column(db.String(100))

    servers = db.relationship('Server', secondary=tbl_user_server,
                              lazy='dynamic',
                              backref=db.backref('servers', lazy='dynamic',
                                                 cascade='delete'))

    @staticmethod
    def register(username: str, email: str, password: str) -> bool:
        # verify password has been properly hashed
        if len(password) != 93:
            raise ValueError('Invalid password value.')
        u = User()
        u.username = username
        u.email = email
        u.password = password
        try:
            db.session.add(u)
            db.session.flush()
            db.session.commit()
            return True
        except IntegrityError as ex:
            db.session.rollback()
            return False

    def join_server_obj(self, server: 'Server'):
        assert(server is not None and server.id is not None)
        if Server is not None:
            try:
                self.servers.append(server)
                db.session.flush()
                db.session.commit()
            except IntegrityError as ex:
                db.session.rollback()
                return False
            return True

    def retrieve_gravatar_and_store(self):
        self.avatar = gravatar.store_gravatar(self)
        db.session.commit()

    @login_manager.user_loader
    def load_user(id):
        return User.query.get(int(id))

    def check_password(self, pword: str):
        return check_password_hash(self.password, pword)

    @property
    def avatar_url(self):
        if self.avatar is None:
            self.retrieve_gravatar_and_store()
        return self.avatar


class Server(DestroyableModel, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('user.id'), index=True,
                         nullable=False)

    owner = db.relationship('User', lazy='select', backref='server_owner')
    name = db.Column(db.String(100), nullable=False)

    # Many-to-many

    # ORM backref for ease of use within Python
    channels = db.relationship('Channel', backref='server_channel',
                               lazy='dynamic')

    users = db.relationship('User', secondary=tbl_user_server,
                            lazy='dynamic',
                            backref=db.backref('server_users', lazy='dynamic',
                                               cascade='delete'))

    @staticmethod
    def create(owner: User, name: str):
        # name length between 1 and 100 chars inclusive
        if 1 <= len(name) <= 100 and User is not None:
            s = Server()
            s.owner_id = owner.id
            s.name = name
            try:
                db.session.add(s)
                db.session.flush()
                db.session.commit()
                db.session.refresh(s)  # ensures id is not none
                # join creator to server
                owner.join_server_obj(s)
                # create initial channel
                c = Channel.create(s)
                return s
            except IntegrityError as ex:
                db.session.rollback()
                return False

    def remove_channel(self, channel: 'Channel'):
        if self.channels.count() == 1:
            return False  # must have at least one channel per server
        channel.destroy()

    def is_authorized(self, user: User, action: str, *args) -> bool:
        '''Checks if user is authorized to perform action.'''
        if action == 'manage-channel':
            return user.id == self.owner_id
        if action == 'see-channels':
            return self.users.filter(User.id == user.id).count() > 0
        if action == 'read-channel':
            return self.users.filter(User.id == user.id).count() > 0
        return False


class Channel(DestroyableModel, db.Model):
    MIN_NAME_LEN = 1
    MAX_NAME_LEN = 30

    server = db.Column(db.Integer,
                       db.ForeignKey('server.id'), primary_key=True)
    name = db.Column(db.String(30), primary_key=True, nullable=False,
                     default='general')

    messages = db.relationship('Message', backref='channel_message',
                               lazy='dynamic', cascade='delete',
                               primaryjoin='and_(Channel.server==Message.server, Channel.name==Message.channel)')

    db.UniqueConstraint('server', 'name', name='uix_channel_server_name')

    @staticmethod
    def create(server: Server, name: str = 'general') \
            -> Union[bool, 'Channel']: # noqa E127
        assert(server is not None)
        # check for existing server
        cexisting = Channel.query.get((server.id, name))
        if cexisting:
            return False
        if Channel.MIN_NAME_LEN <= len(name) <= Channel.MAX_NAME_LEN:
            c = Channel()
            c.server = server.id
            c.name = name
            try:
                db.session.add(c)
                db.session.flush()
                db.session.commit()
                db.session.refresh(c)
                return c
            except IntegrityError as ex:
                db.session.rollback()
                return False
            except Exception:
                db.session.rollback()
                raise


class Message(DestroyableModel, db.Model):
    # FIXME:
    # commented code was supposed to be how you implement
    # composite foreing keys, but didn't work no matter what
    # I tried:
    # __table_args__ = (
    #     db.ForeignKeyConstraint(
    #         ['channel', 'server'],
    #         ['channel.name', 'server.id']
    #     )
    # )
    # BIGINT will ensure trillions of possible messages:
    id = db.Column(db.Integer, primary_key=True)
    # if more messages than that are stored then it will be necessary to
    # switch to a specific database system such as Apache Cassandra due to the
    # sheer amount of traffic trillions of messages would require
    channel = db.Column(db.String(30),
                        # db.ForeignKey('channel.name'),
                        index=True)
    server = db.Column(db.Integer,
                    #    db.ForeignKey('channel.server'),
                       index=True)
    author_id = db.Column(db.Integer,
                          db.ForeignKey('user.id'),
                          index=True)
    author = db.relationship('User', lazy='select', backref='message_author')
    created = db.Column(db.DateTime, nullable=False, default=datetime.utcnow())
    content = db.Column(db.String(3000))

    __table_args__ = (db.ForeignKeyConstraint(['channel', 'server'], ['channel.name', 'channel.server']),)

    def compose(self, channel: Channel, author: User, content: str,
                created: datetime = datetime.utcnow()):
        assert(author is not None and content is not None
               and channel is not None)

        self.channel = channel.name
        self.server = channel.server
        self.author_id = author.id
        self.content = content

        super(Message, self).__init__()

        self.post()

    def post(self) -> Union[bool, int]:
        '''Post a new message to an existing channel.
        Messages must not exceed 3000 characters.

        channel -- Existing channel to post message to.
        '''
        assert(self.channel is not None
               and self.content is not None
               and len(self.content) < 3000)
        try:
            db.session.add(self)
            db.session.flush()
            db.session.commit()
            db.session.refresh(self)
            return self.id
        except IntegrityError as ex:
            db.session.rollback()
            raise ex
            return False

    def edit(self, new_message: str) -> bool:
        '''Edit an existing message's content.
        Messages must not exceed 3000 characters.'''
        assert(self.id is not None and len(new_message) < 3000)
        self.content = new_message
        try:
            db.session.add(self)
            db.session.flush()
            db.session.commit()
            return True
        except IntegrityError as ex:
            db.session.rollback()
            return False

    def destroy(self):
        try:
            db.session.delete(self)
            db.session.flush()
            db.session.commit()
            return True
        except IntegrityError as ex:
            db.session.rollback()
            return False

    def serialize(self):
        '''Return message as JSON for client display.'''
        return {'content': self.content,
                'username': self.author.username,
                'date': self.created.isoformat(),
                'user_id': self.author.id,
                'id': self.id
                }
