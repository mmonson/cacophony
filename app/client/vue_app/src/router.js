import Vue from 'vue'
import Router from 'vue-router'
import App from '@/App.vue'
// import Api from './views/Api.vue'
import Register from '@/views/Register.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'App',
      component: App
    },
    // {
    //   path: '/api',
    //   name: 'api',
    //   component: Api
    // },
    {
      path: '/register',
      name: 'register',
      component: Register
    }
  ]
})
