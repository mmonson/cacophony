import axios from 'axios'
import appState from './app_state'

axios.defaults.withCredentials = true
export default {
  state: {
    messages: []
  },
  sendMessage (content) {
    axios
      .post(appState.server_url + '/api/channel/message', {
        channel: appState.state.active_channel.name,
        server: appState.state.active_server.id,
        content: content
      })
      .then((response) => {
        if (response && response.data) {
          // this.state.messages.push(response.data.message)
        }
      })
      .catch((error) => {
        console.error('error sending message: \n' + error)
      })
    // this.state.messages.push({username, content, date: Date.now()})
  },
  receiveMessage (data) {
    if (data.server === appState.state.active_server.id &&
        data.channel === appState.state.active_channel.name) {
      this.state.messages.push(data.message)
    }
  },

  removeMessage (data) {
    console.log(data)
    if (data.server === appState.state.active_server.id &&
       data.channel === appState.state.active_channel.name) {
      for (var i = 0; i < this.state.messages.length; i++) {
        let m = this.state.messages[i]
        if (m.id === data.message_id) {
          this.state.messages.splice(i, 1)
          return
        }
      }
    }
  },

  destroyMessage (id) {
    axios
      .delete(appState.server_url + '/api/channel/message', {
        params: {
          id: id
        }
      })
      .then(() => {
        return null
      })
      .catch((error) => {
        console.log('error deleting message')
        console.log(error)
      })
  },

  loadInitMessages (server, channel) {
    axios
      .get(appState.server_url + '/api/channel/initmessages', {
        params: {
          channel: channel.name,
          server: server.id
        }
      })
      .then((response) => {
        this.state.messages = []
        if (response.data && response.data.messages) {
          this.state.messages = response.data.messages
        }
      })
      .catch((error) => {
        console.error('error getting messages: \n' + error)
      })
  }
}
