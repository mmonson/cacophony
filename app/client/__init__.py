""" Client App """

import os
from flask import Blueprint, render_template, current_app, redirect

client_bp = Blueprint('client_app', __name__,
                      url_prefix='',
                      static_url_path='',
                      static_folder='./vue_app/dist',
                      template_folder='./vue_app/dist',
                      )

@client_bp.route('/')
def index():
    if current_app.config['PRODUCTION']:
        return redirect("http://cacophony-vue.herokuapp.com"), 303
    else:
        return render_template('index.html')
